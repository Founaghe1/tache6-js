let mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', ];

console.log("sois le tableau suivant");
console.log(mois);

//1- retire la derniere val du tab mois
console.log("1- retire la derniere val du tab mois");
mois.pop();
console.log(mois);

//2- affiche les vals du tab avec la methode document.write
let p = "2- affiche les vals du tab avec la methode document.write";
document.write(p + '<br>');
for (let i = 0; i < mois.length; i++) {
    
    document.write(mois[i] +'<br>');
    
}
console.log("affiche l'index de mars")
console.log(mois.indexOf("mars"))

//3- ajoute la valeur aout a la fin du tab
console.log("3- ajoute la valeur aout a la fin du tab");
mois.push('aout');
console.log(mois);

// 4- Remplacer la valeur 'février' par 'FEVRIER'
console.log("4- Remplacer la valeur 'février' par 'FEVRIER'")

mois[mois.indexOf("février")] = "FEVRIER";
console.log(mois)

mois[mois.indexOf("aout")] = "JUILLET"
console.log(mois)

// 5- Afficher le nombre de valeurs du tableau en utilisant la méthode document.write
let nbVal = "5- Afficher le nombre de valeurs du tableau en utilisant la méthode document.write"

document.write(nbVal + '<br>');
document.write(mois.length + '<br>');

var nbValeurs = 0;
for (var i = 0; i < mois.length; i++) {
nbValeurs++;
}
document.write("Nombre de valeurs dans le tableau : " + nbValeurs);

// 6- Afficher la troisième valeur du tableau
console.log("Afficher la troisième valeur du tableau");
console.log(mois[2]);